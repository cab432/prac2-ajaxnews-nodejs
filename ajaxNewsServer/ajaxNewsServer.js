#!/usr/bin/env node

/*
 * Run:
 *     node ajaxNewsServer.js
 *
 * This script starts a server that serves files in the directory that script resides in.
 * For example, you can view this script from the URL:
 *     http://localhost:8124/ajaxNewsServer.js
 *
 * But you're probably more interested in reading the news headlines, so view the `index.html` page at:
 *     http://localhost:8124/index.html
 *
 * The function that handles requests to the server actually intercepts requests to the root path "/" and retrieves
 * the `index.html` file instead, so you can also access the index at:
 *     http://localhost:8124/
 *
 * The `index.html` page makes requests for the ABC RSS news feed at the path:
 *     /rss.xml
 * which is expanded to:
 *     http://localhost:8124/rss.xml
 * which is a path relative to the domain you're viewing `index.html` on. You can view the raw XML at that path.
 *
 * Read through this script and see how the request handler intercepts requests to "/rss.xml" and performs a request to
 * "http://www.abc.net.au/news/feed/45910/rss.xml" and pipes data from that stream to the client (instead of trying to
 * read the file `rss.xml`).
 *
 * You may also want to look at `index.html` and see how you can use jQuery to make GET requests more easily than with
 * `XMLHttpRequest`. jQuery irons out the cross-browser nuances for you.
 */

var http = require('http');
var url = require('url');
var qs = require('querystring');
var path = require('path');
var fs = require('fs');

var rssURL = "http://www.abc.net.au/news/feed/45910/rss.xml";

// create http server
http.createServer(function (req, res) {

  // Information about the request to the HTTP server
  var urlObj = url.parse(req.url);

  if (urlObj.pathname == '/rss.xml') {
    // If the requested path is '/rss.xml', make a request to the ABC news feed
    // See https://nodejs.org/api/http.html#http_http_request_options_callback for more details on what give `http.request()`
    http.request(rssURL, function(getRes) {
      // This function gets called when the request is successful.
      console.log("Retrieving RSS feed from '" + rssURL + "'");
      res.writeHead(200, { 'content-type': 'text/xml' });
      // Pipe GET response data to the response to the client.
      getRes.pipe(res);

    }).on('error', function(e) {
      // If the URL doesn't work, this function is called.
      res.write("ERROR: Problem retrieving RSS '" + rssURL + "'. " + e);
      res.end();
    }).end(); // Don't forget to end the request.

  } else { // If not proxying, serve up the file.

    // Redirect requests to the root "/" to the index page:
    if (urlObj.path == "/") {
      urlObj.path = "/index.html";
    }

    // __dirname is a variable that Node sets which is the directory that this script resides in.
    // This means that all paths will be relative to the directory containing this script.
    // Also replace instances of '..' with '.' because we don't want people to be able to access files up the filesystem.
    var localPath = path.join(__dirname, urlObj.path).replace(/\.\./g, '.');
    console.log("Serving file '" + localPath + "'");

    // Read the file. You can do this with `fs.readFile()` or `fs.readFileSync()` but let streams do the work for you:
    var fileStream = fs.createReadStream(localPath);
    // Pipe the file data to the client response.
    fileStream.pipe(res);
    // A bit of error handling for if the file doesn't exist...
    fileStream.on('error', function(e) {
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.write("ERROR: Couldn't read file '" + urlObj.path + "'. " + e);
      res.end();
    });

  }

}).listen(8124, function() {
  console.log('bound to port 8124');
});

console.log('Server running on 8124/');