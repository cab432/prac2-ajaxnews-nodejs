#!/usr/bin/env node

/*
 * Run:
 *     node fileServerPlusProxy.js
 *
 * This script starts a server that serves files in the directory that script resides in.
 * For example, you can view this script from the URL:
 *     http://localhost:8124/fileServerPlusProxy.js
 *
 * But you're probably more interested in reading the news headlines, so view the `index.html` page at:
 *     http://localhost:8124/index.html
 *
 * The function that handles requests to the server actually intercepts requests to the root path "/" and retrieves
 * the `index.html` file instead, so you can also access the index at:
 *     http://localhost:8124/
 *
 * The `index.html` page makes requests for the ABC RSS news feed at the path:
 *     /proxy?url=http://www.abc.net.au/news/feed/45910/rss.xml
 * which is expanded to:
 *     http://localhost:8124//proxy?url=http://www.abc.net.au/news/feed/45910/rss.xml
 * which is a path relative to the domain you're viewing `index.html` on.
 * This URL functions much like the PHP script `simple-proxy.php` (except this script is much less robust).
 *
 * Read through this script and see how the request handler intercepts requests to "/proxy" and performs a request to
 * the URL passed as a query parameter "/proxy?url=<URL>" and pipes data from that stream to the client.
 *
 * You may also want to look at `index.html` and see how you can use jQuery to make GET requests more easily than with
 * `XMLHttpRequest`. jQuery irons out the cross-browser nuances for you.
 */

var http = require('http');
var url = require('url');
var qs = require('querystring');
var path = require('path');
var fs = require('fs');

// create http server
http.createServer(function (req, res) {

  // Information about the request to the HTTP server
  var urlObj = url.parse(req.url);
  var params = qs.parse(urlObj.query);

  if (urlObj.pathname == '/proxy') { // The URL path '/proxy' will be a proxy endpoint.

    // But only requests with a 'url' parameter e.g. '/proxy?url=http://www.abc.net.au/news/feed/45910/rss.xml'
    if (params.url != null) {

      // Create a request information object based on the URL to be GET'd.
      var remoteUrlObj = url.parse(params.url);
      // Forward the request headers and method from client request to the GET request.
      remoteUrlObj.method = req.method;
      remoteUrlObj.headers = req.headers;
      // Remove the 'host' header because it still refers to the proxy host 'localhost:8124'
      delete remoteUrlObj.headers.host;
      // See https://nodejs.org/api/http.html#http_http_request_options_callback for more details on what to put in remoteUrlObj

      // Perform the request...
      http.request(remoteUrlObj, function(getRes) {
        console.log("Proxying '" + params.url + "'");
        // Forward the response headers from the GET request to the client response.
        res.writeHead(getRes.statusCode, getRes.headers);
        // Pipe GET response data to client response.
        getRes.pipe(res);

      }).on('error', function(e) {
        // If the URL doesn't work...
        res.write("ERROR: Problem retrieving address '" + params.url + "' through proxy.");
        res.end();
      }).end(); // Don't forget to end the request.

    } else {
      // Don't forget to specify a URL for the proxy!
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.write("ERROR: No URL given. Use proxy as '/proxy?url=[URL]'.");
      res.end();
    }

  } else { // If not proxying, serve up the file.

    // Redirect requests to the root "/" to the index page:
    if (urlObj.path == "/") {
      urlObj.path = "/index.html";
    }

    // __dirname is a variable that Node sets which is the directory that this script resides in.
    // This means that all paths will be relative to the directory containing this script.
    // Also replace instances of '..' with '.' because we don't want people to be able to access files up the filesystem.
    var localPath = path.join(__dirname, urlObj.path).replace(/\.\./g, '.');
    console.log("Serving file '" + localPath + "'");

    // Read the file. You can do this with `fs.readFile()` or `fs.readFileSync()` but let streams do the work for you:
    var fileStream = fs.createReadStream(localPath);
    // Pipe the file data to the client response.
    fileStream.pipe(res);
    // A bit of error handling for if the file doesn't exist...
    fileStream.on('error', function(e) {
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.write("ERROR: Couldn't read file '" + urlObj.path + "'. " + e);
      res.end();
    });

  }

}).listen(8124, function() {
  console.log('bound to port 8124');
});

console.log('Server running on 8124/');