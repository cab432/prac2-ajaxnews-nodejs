# CAB432 - Practical 2 - Ajax News (Node version)

In Practical 2 you had the chance to experiment with jQuery and proxying 
 requests with a PHP script served by an XAMPP stack, and also serving
 static files with a Node.js server.

This repository contains two versions of the Ajax News example that are
 self-contained Node scripts that both serve files and proxy requests (no
 Apache or PHP required).

  1. The folder `ajaxNewsServer` contains a Node script that serves files and 
     redirects requests to "/rss.xml" to the ABC news RSS feed.
  2. The folder `fileServerPlusProxy` contains a Node script that serves files
     and redirects requests to "/proxy?url=<URL>" to the given URL.

Both these folders contain `index.html` files that display the ABC news
 headlines, but with two differences from the original:
 
  1. jQuery is used to manipulate DOM elements.
  2. jQuery is used to perform GET requests to the RSS feed.

Run the servers as usual, i.e.:

    node ajaxNewsServer.js

Or:

    node fileServerPlusProxy.js

And then navigate to:

    http://localhost:8124

Read the comments in the file.